package net.downwithdestruction.dwdmoblimiter.runners;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Entity;

import net.downwithdestruction.dwdmoblimiter.DwDMobLimiter;

public class CheckCount implements Runnable {
	private DwDMobLimiter plugin;
	private HashMap<String,Integer> count = new HashMap<String,Integer>();
	
	public CheckCount(DwDMobLimiter plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void run() {
		if (!plugin.enabled) return;
		for (World world : plugin.getServer().getWorlds()) {
			for (Entity hostEntity : world.getEntities()) {
				final String key = hostEntity.getType().toString();
				count.put(key, 0);
				for (Entity entity : hostEntity.getNearbyEntities(plugin.radius, plugin.radius, plugin.radius)) {
					if (!entity.getType().toString().equalsIgnoreCase(key)) continue;
					count.put(key, count.get(key) + 1);
				}
				final Integer maximum = plugin.max.get(key);
				final String override = plugin.config.getString("override." + world + "." + key.toLowerCase(), null);
				Integer maxVal;
				try {
					maxVal = (override != null) ? Integer.valueOf(override) : maximum;
				} catch (NumberFormatException nfe) {
					maxVal = maximum;
				}
				if (maxVal >= 0) {
					if (count.get(key) >= maxVal) {
						hostEntity.remove();
						if (plugin.debug)
							plugin.log(ChatColor.AQUA + "REMOVED Creature from [" +
									ChatColor.DARK_AQUA + world.getName() +
									ChatColor.AQUA + "]: " +
									ChatColor.DARK_AQUA + hostEntity.getType() +
									ChatColor.AQUA + " (" +
									ChatColor.DARK_AQUA + ((override != null) ? "override (" + world + ")" : "maximum") +
									ChatColor.AQUA + ": " +
									ChatColor.DARK_AQUA + maxVal +
									ChatColor.AQUA + ")");
					}
				}
			}
		}
	}
}
