package net.downwithdestruction.dwdmoblimiter.listeners;

import java.util.HashMap;

import net.downwithdestruction.dwdmoblimiter.DwDMobLimiter;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class EntityListener implements Listener {
	private DwDMobLimiter plugin;
	private HashMap<String,Integer> count = new HashMap<String,Integer>();
	
	public EntityListener(DwDMobLimiter plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onCreatureSpawnEvent (CreatureSpawnEvent event) {
		if (!plugin.enabled) return;
		final String key = event.getEntityType().toString();
		count.put(key, 0);
		for (Entity entity : event.getEntity().getNearbyEntities(plugin.radius, plugin.radius, plugin.radius)) {
			if (!entity.getType().toString().equalsIgnoreCase(key)) continue;
			count.put(key, count.get(key) + 1);
		}
		final String world = event.getEntity().getWorld().getName();
		final Integer maximum = plugin.max.get(key);
		final String override = plugin.config.getString("override." + world + "." + key.toLowerCase(), null);
		Integer maxVal;
		try {
			maxVal = (override != null) ? Integer.valueOf(override) : maximum;
		} catch (NumberFormatException nfe) {
			maxVal = maximum;
		}
		if (maxVal >= 0) {
			if (count.get(key) >= maxVal) {
				event.setCancelled(true);
				if (plugin.debug)
					plugin.log(ChatColor.AQUA + "BLOCKED Creature Spawn Event: " +
							ChatColor.DARK_AQUA + event.getEntityType() +
							ChatColor.AQUA + " (" +
							ChatColor.DARK_AQUA + ((override != null) ? "override (" + world + ")" : "maximum") +
							ChatColor.AQUA + ": " +
							ChatColor.DARK_AQUA + maxVal +
							ChatColor.AQUA + ")");
				return;
			}
		}
	}
}
