package net.downwithdestruction.dwdmoblimiter;

import java.util.HashMap;
import java.util.logging.Level;

import net.downwithdestruction.dwdmoblimiter.commands.CmdDwDMobLimiter;
import net.downwithdestruction.dwdmoblimiter.listeners.EntityListener;
import net.downwithdestruction.dwdmoblimiter.runners.CheckCount;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DwDMobLimiter extends JavaPlugin {
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	private Integer checkCountTaskId;
	
	public Boolean debug = false;
	public Boolean colorLogs = true;
	public Boolean enabled = false;
	public Double radius;
	public Integer checkDelay;
	public HashMap<String,Integer> max = new HashMap<String,Integer>();
	public FileConfiguration config;
	
	public void onEnable() {
		loadConfig();
		startCheckCountTask();
		pluginManager.registerEvents(new EntityListener(this), this);
		getCommand("dwdmoblimiter").setExecutor(new CmdDwDMobLimiter(this));
		log(ChatColor.AQUA + "v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log(ChatColor.AQUA + "Plugin Disabled.");
	}
	
	public void loadConfig() {
		config = getConfig();
		if (!config.contains("debug-mode")) config.addDefault("debug-mode", false);
		if (!config.contains("color-logs")) config.addDefault("color-logs", true);
		if (!config.contains("enabled")) config.addDefault("enabled", false);
		if (!config.contains("radius")) config.addDefault("radius", 8D);
		if (!config.contains("check-delay")) config.addDefault("check-delay", 20);
		for (EntityType type : EntityType.values())
			if (!config.contains("maximum." + type.toString().toLowerCase()))
				config.addDefault("maximum." + type.toString().toLowerCase(), -1);
		if (!config.contains("override")) {
			config.addDefault("override.world.sheep", 25);
			config.addDefault("override.world2.slimes", 15);
		}
		config.options().copyDefaults(true);
		saveConfig();
		debug = config.getBoolean("debug-mode", false);
		colorLogs = getConfig().getBoolean("color-logs", true);
		enabled = config.getBoolean("enabled", false);
		radius = config.getDouble("radius", 8D);
		checkDelay = config.getInt("check-delay", 20);
		max.clear();
		for (EntityType type : EntityType.values())
			max.put(type.toString(), config.getInt("maximum." + type.toString().toLowerCase(), -1));
	}
	
	public void startCheckCountTask() {
		checkCountTaskId = getServer().getScheduler().scheduleSyncRepeatingTask(this, new CheckCount(this), 0, checkDelay);
	}
	
	public void stopCheckCountTask() {
		Bukkit.getScheduler().cancelTask(checkCountTaskId);
	}
	
	public void log (final Object obj) {
		if (colorLogs) {
			getServer().getConsoleSender().sendMessage(
					ChatColor.AQUA + "[" + ChatColor.LIGHT_PURPLE + getName() + ChatColor.AQUA + "] " + ChatColor.RESET + obj
			);
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
}
