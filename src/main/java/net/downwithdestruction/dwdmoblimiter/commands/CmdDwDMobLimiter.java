package net.downwithdestruction.dwdmoblimiter.commands;

import net.downwithdestruction.dwdmoblimiter.DwDMobLimiter;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdDwDMobLimiter implements CommandExecutor {
	private DwDMobLimiter plugin;
	
	public CmdDwDMobLimiter(DwDMobLimiter plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("dwdmoblimiter")) {
			if (sender instanceof Player) {
				sender.sendMessage(ChatColor.RED + "This command is a console-only command.");
				return true;
			}
			if (args.length < 1) {
				sender.sendMessage(ChatColor.GREEN + "DwDMobLimiter version: v" + plugin.getDescription().getVersion().toString());
				return true;
			}
			if (args[0].equalsIgnoreCase("reload")) {
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Reloading config.yml");
				plugin.reloadConfig();
				plugin.loadConfig();
				if(plugin.debug) plugin.log(ChatColor.GREEN + "Restarting CheckCountTask");
				plugin.stopCheckCountTask();
				plugin.startCheckCountTask();
				sender.sendMessage(ChatColor.GREEN + "DwDMobLimiter config.yml has been reloaded.");
				return true;
			}
		}
		return false;
	}
}
